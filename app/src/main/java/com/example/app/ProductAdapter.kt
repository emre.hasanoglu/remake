package com.example.app

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class ProductAdapter(product : List<Product>) : RecyclerView.Adapter<ProductViewHolder>() {

    var productList = product
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        return ProductViewHolder(parent)
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        holder.bindTo(productList[position])

    }

    override fun getItemCount(): Int {
        return productList.size
    }
}