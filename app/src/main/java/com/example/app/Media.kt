package com.example.app

data class Media(
    var type : String,
    var path : String,
    var extra : String
)