package com.example.app

import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

class ProductDocumentAdapter(documentList: List<Document>) : RecyclerView.Adapter<ProductDocumentAdapter.ProductDetailViewHolder>() {

    var allProductSpecialty = documentList

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductDetailViewHolder {
        var inflater = LayoutInflater.from(parent.context)
        var details = inflater.inflate(R.layout.item_attachment_blue, parent, false)

        return  ProductDetailViewHolder(details)
    }

    override fun onBindViewHolder(holder : ProductDetailViewHolder, position : Int) {

        var detail = allProductSpecialty[position]
        holder.bindTo(detail)
    }

    override fun getItemCount(): Int {
        return allProductSpecialty.size
    }

    inner class  ProductDetailViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {

        val BASE_URL = "https://tarimalani.bayer.com.tr"
        val productAttachment = itemView.findViewById<AppCompatButton>(R.id.btn_attachment)
        val productImage = itemView.findViewById<ImageView>(R.id.image_product)
//        val productName = itemView.findViewById<AppCompatTextView>(R.id.txt_product_name)
//        val textViewSlogan = itemView.findViewById<AppCompatTextView>(R.id.tv_slogan)
//        val textProductGroup = itemView.findViewById<AppCompatTextView>(R.id.text_of_product_group)
//        val activeSubstance = itemView.findViewById<AppCompatTextView>(R.id.active_substace)
//        val textFormulation = itemView.findViewById<AppCompatTextView>(R.id.text_formulation)
//        val packageSize = itemView.findViewById<AppCompatTextView>(R.id.package_size)
//
//        fun bindTo(detail : ProductSpecialty) {
//            Glide.with(itemView.context).load(BASE_URL + detail.image).into(productImage)
//            productName.text = detail.name
//            textViewSlogan.text = detail.slogan
//            textProductGroup.text = detail.specialty.group
//            activeSubstance.text = detail.specialty.activeSubstance
//            textFormulation.text = detail.specialty.formulation
//            packageSize.text = detail.specialty.packageSizes
//        }
//    }

        fun bindTo(detail : Document) {
            productAttachment.text = detail.name

            productAttachment.setOnClickListener {
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(BASE_URL + detail.path))
                it.context.startActivity(intent)
            }
        }
    }
}