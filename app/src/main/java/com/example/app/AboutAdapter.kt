package com.example.app

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class AboutAdapter(aboutList : List<AboutResponse>) : RecyclerView.Adapter<AboutViewHolder>() {

    var aboutDetailList: List<AboutResponse>

    init {
        aboutDetailList = aboutList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AboutViewHolder {
        return AboutViewHolder(parent)
    }

    override fun onBindViewHolder(holder: AboutViewHolder, position: Int) {
        holder.bindTo(aboutDetailList[position])
    }

    override fun getItemCount(): Int {
        return aboutDetailList.size
    }

}