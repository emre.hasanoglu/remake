package com.example.app

data class AboutDetailResponse(
    val about : List<AboutResponse>
)