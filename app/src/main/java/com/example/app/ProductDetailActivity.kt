package com.example.app

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_product_detail.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProductDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_detail)

        val intent = intent
        var id = intent.getStringExtra("productId")

        recycler_attachments.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        ApiClient.getClient().create(ApiInterface::class.java)
            .loadProductDetails(id).enqueue(object : Callback<ProductSpecialty> {

                override fun onResponse(call: Call<ProductSpecialty>, response: Response<ProductSpecialty>) {
                    var productFeature = response.body()
                    var productDocument = response.body()?.document
                    var speciality = response.body()?.specialty
                    var content = response.body()?.content
                    var media = response.body()?.media

                    recycler_attachments.adapter = ProductDocumentAdapter(productDocument!!)
                    recycler_features.adapter = ProductContentAdapter(content!!)
                    recyler_media.adapter = ProductMediaAdapter(media!!)

                    txt_product_name.text = productFeature?.name
                        ?.replace("<sup>&reg;</sup>", "®")
                    tv_slogan.text = productFeature?.slogan

                    text_of_product_group.text = speciality?.group
                    active_substace.text = speciality?.activeSubstance
                    text_formulation.text = speciality?.formulation
                    package_size.text = speciality?.packageSizes

                }

                override fun onFailure(call: Call<ProductSpecialty>, t: Throwable) {
                    Toast.makeText(this@ProductDetailActivity, "Failure!" + t.message, Toast.LENGTH_SHORT).show()
                }
            })

        detail_back.setOnClickListener {
            onBackPressed()
        }
    }
}
