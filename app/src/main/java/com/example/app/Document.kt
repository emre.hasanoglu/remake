package com.example.app

data class Document(

    var name : String,
    var path : String
)