package com.example.app

import android.content.Intent
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity

class Splash : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        showProductDialog()
    }

    private fun showProductDialog() {
        val builder = AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle)
        builder.setMessage("Ürünlerimizi kullanmadan önce mutlaka etiketini okuyunuz.")
        builder.setCancelable(false)
        builder.setPositiveButton("TAMAM") { dialog, _ ->
            dialog.dismiss()
            startActivity(Intent(this@Splash, MainActivity::class.java))
            finish()
        }
        builder.show()
    }


}