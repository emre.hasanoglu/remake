package com.example.app

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_program.*
import retrofit2.Call
import retrofit2.Response

class Program : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_program)

        recyler_program.layoutManager = GridLayoutManager(this, 2)
        ApiClient.getClient().create(ApiInterface::class.java)
            .loadChemicals().enqueue(object : retrofit2.Callback<List<ProductGroup>> {
                override fun onResponse(call: Call<List<ProductGroup>>, response: Response<List<ProductGroup>>) {
                    var productList = response.body()

                    recyler_program.adapter = productList?.let { ChemicalAdapter(it) }
                }

                override fun onFailure(call: Call<List<ProductGroup>>, t: Throwable) {
                    Toast.makeText(this@Program, "Failure!", Toast.LENGTH_SHORT).show()
                }

            })

        program_back.setOnClickListener {
            onBackPressed()
        }
    }
}