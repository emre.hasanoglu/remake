package com.example.app

data class Content(
    var header : String,
    var data : List<InnerData>,
    var isExpanded : Boolean = false
)