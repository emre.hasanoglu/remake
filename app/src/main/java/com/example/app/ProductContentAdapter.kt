package com.example.app

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.view.children
import androidx.core.view.isNotEmpty
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.example.app.utils.MWebView
import kotlinx.android.synthetic.main.item_content.view.*
import kotlinx.android.synthetic.main.mwebview.view.*


class ProductContentAdapter(content : List<Content>) : RecyclerView.Adapter<ProductContentAdapter.ProductContentViewholder>() {

    var productContent = content

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductContentViewholder {
        var inflater = LayoutInflater.from(parent.context)
        var index = inflater.inflate(R.layout.item_content, parent, false)

        return ProductContentViewholder(index)
    }

    override fun onBindViewHolder(holder: ProductContentViewholder, position: Int) {
        var contentDetail = productContent[position]
        holder.bindTo(contentDetail)
    }

    override fun getItemCount(): Int {
        return productContent.size
    }

    inner class ProductContentViewholder(itemview: View) : RecyclerView.ViewHolder(itemview) {
        val buttonExpand = itemview.findViewById<Button>(R.id.btn_expand)
        val descTextView = itemview.findViewById<TextView>(R.id.txtView_standard)
        val layoutInner = itemview.findViewById<LinearLayout>(R.id.layout_inner)

        fun bindTo(contentDetail: Content) {
            buttonExpand.text = contentDetail.header
            for (prodDesc in contentDetail.data) {
                for (pd in prodDesc.data) {
                    if (prodDesc.type.equals("list")) {
                        descTextView.append(pd + "\n")
                        buttonExpand.setOnClickListener {
                            var isOpen = descTextView.isVisible
                            closeAllItems(itemView)
                            switchVisibility(descTextView, isOpen)
                        }
                    } else if (prodDesc.type.equals("paragraph")) {
                        descTextView.append(pd + "\n")
                        buttonExpand.setOnClickListener {
                            var isOpen = descTextView.isVisible
                            closeAllItems(itemView)
                            switchVisibility(descTextView, isOpen)
//                var descTextView = (it.parent as View).txtView_standard
                        }
                    } else if (prodDesc.type.equals("table")) {
                        val builder = StringBuilder()
                        builder.append("<!doctype html>")
                        builder.append("<html lang=\"en\">")
                        builder.append("<head>")
                        builder.append("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">")
                        builder.append("</head><body>")
                        builder.append(pd)
                        builder.append("</body></html>")
                        val mWebView = LayoutInflater.from(itemView.context).inflate(R.layout.mwebview, layoutInner, false) as MWebView
                        layoutInner.addView(mWebView)
//                        mWebView.loadData(builder.toString(), "text/html; charset=UTF-8", null)
                        mWebView.loadDataWithBaseURL(null,builder.toString(),"text/html", "utf-8", null)
                        buttonExpand.setOnClickListener {
                            var isOpen = mWebView.isVisible
                            closeAllItems(itemView)
                            switchVisibility(mWebView, isOpen)
                        }
                    }
                }
            }
        }
    }

    fun closeAllItems(itemView: View) {
        for (iv in (itemView.parent as RecyclerView).children) {
            iv.txtView_standard.visibility = View.GONE
            if (iv.webview != null) {
                iv.webview.visibility = View.GONE
            }
        }
    }

    fun switchVisibility(view : View, isOpen: Boolean) {
        if (isOpen) {
            view.visibility = View.GONE
        } else {
            view.visibility = View.VISIBLE
        }
    }
}
