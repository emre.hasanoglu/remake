package com.example.app

import com.google.gson.annotations.SerializedName



data class InnerData(
    var type : String,
    var data : List<String>
)