package com.example.app

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ApiClient {
    companion object {
        fun getClient(): Retrofit {
            return Retrofit.Builder()
                .baseUrl("https://tarimalani.bayer.com.tr")
                .addConverterFactory(GsonConverterFactory.create()).build()
        }
    }
}