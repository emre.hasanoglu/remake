package com.example.app

data class Speciality(

    var group : String,
    var activeSubstance : String,
    var formulation : String,
    var packageSizes : String
)