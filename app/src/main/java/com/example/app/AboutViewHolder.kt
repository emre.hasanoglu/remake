package com.example.app

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.widget.AppCompatButton
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import java.lang.StringBuilder

class AboutViewHolder(viewGroup: ViewGroup):RecyclerView.ViewHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.item_content, viewGroup, false)) {

    private val buttonExpand = itemView.findViewById(R.id.btn_expand) as Button
//    private val descStringBuilder = StringBuilder()
//    private val innerLayout = itemView.findViewById(R.id.inner_layout) as LinearLayout
//    private val subTextView = itemView.findViewById(R.id.txtView_subtitle) as TextView
    private val stdTextView = itemView.findViewById(R.id.txtView_standard) as TextView


    fun bindTo(aboutDto : AboutResponse) {

//        if (aboutDto.type == "main") {
//            buttonExpand.text = aboutDto.content
//        }
//        if(aboutDto.type == "standard") {
//            stdTextView.append(aboutDto.content)
//            stdTextView.append("\n")
//        }
//        if(aboutDto.type == "subtitle") {
//            stdTextView.append("\n")
//            stdTextView.text = aboutDto.content
//            stdTextView.append("\n")
//        }



        buttonExpand.setOnClickListener {
            if(stdTextView.isVisible) {
                stdTextView.visibility = View.GONE
            } else {
                stdTextView.visibility = View.VISIBLE
            }
        }
    }
}