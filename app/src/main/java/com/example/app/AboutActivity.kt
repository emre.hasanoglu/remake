package com.example.app

import android.os.Bundle
import android.text.Layout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageButton
import androidx.core.view.isVisible
import kotlinx.android.synthetic.main.activity_about.*
import kotlinx.android.synthetic.main.item_content.*
import kotlinx.android.synthetic.main.item_content.view.*
import kotlinx.android.synthetic.main.item_content.view.btn_expand
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.zip.Inflater

class AboutActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)

        ApiClient.getClient().create(ApiInterface::class.java)
            .loadAbout().enqueue(object : Callback<List<AboutResponse>> {
                override fun onResponse(call: Call<List<AboutResponse>>, response: Response<List<AboutResponse>>) {
                    var aboutList = response.body()
                    fillContent(aboutList!!)
                }

                override fun onFailure(call: Call<List<AboutResponse>>, t: Throwable) {
                    Toast.makeText(this@AboutActivity, "There were things here, they're gone now, apparently", Toast.LENGTH_SHORT).show()
                }
            })

        var buttonBack = findViewById<AppCompatImageButton>(R.id.back)

        buttonBack.setOnClickListener {
            onBackPressed()
        }
    }

    fun fillContent(aboutList: List<AboutResponse>) {
        var itemContentView: View? = null
        for (about in aboutList) {
            if (about.type.equals("main")) {
                itemContentView = LayoutInflater.from(this@AboutActivity).inflate(R.layout.item_content, null)
                itemContentView!!.btn_expand.append(about.content)
                itemContentView.btn_expand.setOnClickListener {
                    var descTextView = (it.parent as View).txtView_standard
                    if (descTextView.isVisible) {
                       descTextView.visibility = View.GONE
                    } else {
                        descTextView.visibility = View.VISIBLE
                    }
                }
                about_scroll.addView(itemContentView)
            } else if (about.type.equals("standard")) {
                itemContentView!!.txtView_standard.append(about.content)
                itemContentView.txtView_standard.append("\n")
            } else if (about.type.equals("subtitle")) {
                itemContentView!!.txtView_standard.append("\n")
                itemContentView.txtView_standard.append(about.content)
                itemContentView.txtView_standard.append("\n\n")

            }
        }

    }
}