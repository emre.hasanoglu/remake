package com.example.app

data class ProductResponse(
    val product : List<ProductGroup>
)