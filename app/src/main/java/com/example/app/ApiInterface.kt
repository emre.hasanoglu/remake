package com.example.app

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiInterface {
    @GET("/scripts/components/product_groups/page_166.php")
    fun loadChemicals() : Call<List<ProductGroup>>

    @GET("/scripts/components/static/page_182.php")
    fun loadAbout() : Call<List<AboutResponse>>

    @GET("/scripts/include/custom/productsSearch.php")
    fun loadProducts() : Call<List<Product>>

    @GET("/scripts/components/products/product_{id}.php")
    fun loadProductDetails(@Path("id") id : String) : Call<ProductSpecialty>
}