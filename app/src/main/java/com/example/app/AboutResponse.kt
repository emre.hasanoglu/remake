package com.example.app

data class AboutResponse(
    var item : String,
    var type : String,
    var content : String
)