package com.example.app

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class ChemicalAdapter(productList : List<ProductGroup>): RecyclerView.Adapter<ChemicalViewHolder>() {

    var productList = productList
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChemicalViewHolder {
        return ChemicalViewHolder(parent )
    }

    override fun onBindViewHolder(holder: ChemicalViewHolder, position: Int) {
        holder.bindTo(productList[position])
    }

    override fun getItemCount(): Int {
        return productList.size
    }

}

