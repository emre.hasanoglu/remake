package com.example.app

import com.google.gson.annotations.SerializedName

data class Product(
    var name : String,
    var plantCategory : String,
    var img : String,
    @SerializedName("product_id") var productId : String

)