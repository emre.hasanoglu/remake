package com.example.app

data class ProductSpecialty(
    var name : String,
    var image : String,
    var slogan : String,
    var specialty : Speciality,
    var document : List<Document>,
    var content : List<Content>,
    var media : List<Media>
)
