package com.example.app

data class ProductGroup (
    val title: String,
    val image: String,
    val link : String
)