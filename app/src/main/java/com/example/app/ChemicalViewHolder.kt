package com.example.app

import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

class ChemicalViewHolder(viewGroup: ViewGroup) : RecyclerView.ViewHolder (LayoutInflater.from(viewGroup.context).inflate(R.layout.item_program, viewGroup, false)) {

    private val BASE_URL = "https://tarimalani.bayer.com.tr"
    private val imageViewPlant = itemView.findViewById(R.id.imageView_plant) as AppCompatImageView
    private val textViewPlant = itemView.findViewById(R.id.textView_plant) as AppCompatTextView
    private val itemContainer = itemView.findViewById(R.id.itemContainer) as LinearLayout
    fun bindTo(productDto:ProductGroup) {
        textViewPlant.text=productDto.title
        Glide.with(itemView.context).load(BASE_URL + productDto.image).into(imageViewPlant)

        itemContainer.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(BASE_URL + productDto.link))
            it.context.startActivity(intent)
        }

    }

}
