package com.example.app

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

class ProductMediaAdapter(media : List<Media>) : RecyclerView.Adapter<ProductMediaAdapter.ProductMediaViewHolder>() {

    var productMedia = media
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductMediaViewHolder {
        var inflater = LayoutInflater.from(parent.context)
        var index = inflater.inflate(R.layout.item_media, parent, false)

        return ProductMediaViewHolder(index)
    }

    override fun onBindViewHolder(holder: ProductMediaViewHolder, position: Int) {
        var mediDetail = productMedia[position]
        holder.bindTo(mediDetail)
    }

    override fun getItemCount(): Int {
        return productMedia.size
    }

    inner class ProductMediaViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {
        val BASE_URL = "https://tarimalani.bayer.com.tr"
        val mediaImage = itemView.findViewById<ImageView>(R.id.imageView)

        fun bindTo(media : Media) {
            Glide.with(itemView.context).load(BASE_URL + media.path).into(mediaImage)
        }
    }


}