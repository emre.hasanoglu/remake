package com.example.app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.LinearLayout
import androidx.appcompat.widget.AppCompatImageButton

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var infoButton = findViewById<AppCompatImageButton>(R.id.info)
        infoButton.setOnClickListener {
            val intent = Intent(this@MainActivity, AboutActivity::class.java)
            startActivity(intent)
        }

        var calendarButton = findViewById<AppCompatImageButton>(R.id.calendar)
        calendarButton.setOnClickListener {
            val intent = Intent(this@MainActivity, Program::class.java)
            startActivity(intent)
        }

        var bayerProduct = findViewById<LinearLayout>(R.id.linear_listing)
        bayerProduct.setOnClickListener {
            val intent = Intent(this@MainActivity, AllProductsActivity::class.java)
            startActivity(intent)
        }
    }


}
