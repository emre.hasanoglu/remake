package com.example.app

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.EditText
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

class ProductViewHolder (viewGroup: ViewGroup) : RecyclerView.ViewHolder (LayoutInflater.from(viewGroup.context).inflate(R.layout.item_product, viewGroup, false)) {

    private val BASE_URL = "https://tarimalani.bayer.com.tr"
    private val textSearch = itemView.findViewById<EditText>(R.id.et_search)
    private val imageViewProduct = itemView.findViewById<AppCompatImageView>(R.id.product_image)
    private val textViewProductCategory = itemView.findViewById<AppCompatTextView>(R.id.product_category)
    private val textViewProductName = itemView.findViewById<AppCompatTextView>(R.id.product_name)
    private val body = itemView.findViewById<ConstraintLayout>(R.id.constraint_item_product)

    fun bindTo(productDto : Product) {
        textViewProductCategory.text = productDto.plantCategory
        textViewProductName.text = productDto.name
            .replace("<sup>&reg;</sup>", "®")
        Glide.with(itemView.context).load(BASE_URL + productDto.img).into(imageViewProduct)

        body.setOnClickListener { v ->
            val intent = Intent(v.context, ProductDetailActivity::class.java)
            intent.putExtra("productId", productDto.productId)
            v.context.startActivity(intent)
        }
    }
}