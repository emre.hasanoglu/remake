package com.example.app


import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_allproducts.*
import retrofit2.Call
import retrofit2.Response

class AllProductsActivity : AppCompatActivity() {

        lateinit var productList : List<Product>
        lateinit var searchedProductList : ArrayList<Product>

        override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_allproducts)
            searchedProductList = ArrayList()
        recycler_products.layoutManager = LinearLayoutManager(this)
        ApiClient.getClient().create(ApiInterface::class.java)
            .loadProducts()
            .enqueue(object : retrofit2.Callback<List<Product>> {
                override fun onResponse(
                    call: Call<List<Product>>,
                    response: Response<List<Product>>
                ) {
                    productList = response.body()!!
                    searchedProductList.addAll(productList)

                    recycler_products.adapter = searchedProductList?.let { ProductAdapter(it) }
                }

                override fun onFailure(call: Call<List<Product>>, t: Throwable) {
                    Toast.makeText(this@AllProductsActivity, "Failure!", Toast.LENGTH_SHORT).show()
                }
            })

            et_search.addTextChangedListener(object : TextWatcher{
                override fun afterTextChanged(s: Editable?) {

                }

                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    searchedProductList.clear()
                    //searchedProductList.addAll()  servisten gelen datayı
                    for(product in productList) {
                        if(product.name.contains(s.toString(), true) || product.plantCategory.contains(s.toString(), true)){
                            //gelen verileri listeye at, recyclerview güncelle
                            searchedProductList.add(product)
                        }
                    }
                    recycler_products.adapter!!.notifyDataSetChanged()
                    println(s.toString())
                }

            })

        product_back.setOnClickListener {
            onBackPressed()
        }
    }

    override fun onStart() {
        super.onStart()
    }
}